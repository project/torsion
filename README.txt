Torsion

Basic install instructions:

1. Place the ‘UI’ folder wherever you like. We normally have this outside of the Drupal root folder, and just a sibling folder to it.

2. Place the ‘themes’ folder wherever you place your custom themes. Make note of where you’ve placed this folder, and go into the ‘gulpfile.js’ file, and change the “var theme path” to reflect where you’ve put the torsion folder.

Basic use instructions:

1. To install the dependencies, run “npm install”. Occasionally on some system this does not complete the ‘bower’ portion and you may need to run “bower install” manually thereafter.

There are two basic gulp commands to compile the front-end theme.

a. ‘gulp dev’.  This is suitable for any local development, it compiles the css and JS so that commenting is maintained, and is easy to debug.

b. ‘gulp build’.  This is for production (or before committing your project to the repo). This will uglily and compress everything and remove all comments and such.